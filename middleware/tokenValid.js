const jwt = require("jsonwebtoken")
const logger = require("./logging")
const { JWT_SECRET } = process.env

const tokenValidator = (req, res, next) => {
    try {
        const token = req.headers.authorization
        if (!token || !token.startsWith('Bearer ')) {
            logger.error('Invalid token')
            return res.status(401).json({ msg: 'Invalid token' })
        }
        const tokenString = token.slice(7)
        jwt.verify(tokenString, JWT_SECRET, (err, decode) => {
            if (err) {
                console.log({ err });
                logger.warn('Unauthorized request')
                res.status(401).json({ msg: 'Unauthorized request' })
            } else {
                req.user = decode
                next()
            }
        })
    } catch (error) {
        logger.error('Error in token validation', error)
        res.status(500).json({ msg: 'Error in token validation' })
    }
}
module.exports = tokenValidator