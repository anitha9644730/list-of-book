const winston = require('winston')
const DailyRotateFile = require('winston-daily-rotate-file')

const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.prettyPrint()),
    transports: [
        new DailyRotateFile({
            dirname: 'logs',
            filename: '%DATE%.log',
            datePattern: 'DD-MM-YYYY',
            maxFiles: '14d'
        }),
        new winston.transports.Console()
    ]
});

module.exports = logger