const { User } = require('../model')
const { genSaltSync, hashSync, compareSync } = require('bcrypt')
const { JWT_SECRET } = process.env
const jwt = require('jsonwebtoken')
const { logger } = require('../middleware')


class userController { }
userController.register = async (req, res) => {
    let { body: { email, mobile, password }, body } = req
    try {
        let found = await User.findOne({ $or: [{ mobile }, { email }] })
        if (!found) {
            body.password = hashSync(password, genSaltSync(10))
            let user = await User.create(body)
            logger.info('Register Successfully');
            res.status(201).json({ msg: 'Register Successfully', data: user })
        } else if (found?.email === email && found?.mobile === mobile) {
            logger.warning('Already Existed Account. Please Login')
            res.status(208).json({ msg: 'Already Existed Account. Please Login' })
        }
        else if (found?.email === email) {
            logger.warning('Already Existed Email Id')
            res.status(208).json({ msg: 'Already Existed Email Id' })
        }
        else {
            logger.error('Already Existed Mobile Number')
            res.status(208).json({ msg: 'Already Existed Mobile Number' })
        }
    } catch (e) {
        logger.error('Error In Register')
        res.send('Error In Register')
    }
}
userController.login = async (req, res) => {
    const { body: { name, password } } = req
    try {
        const found = await User.findOne({ $or: [{ mobile: name }, { email: name }] })
        logger.info({ found })

        if (found) {
            if (compareSync(password, found?.password)) {
                const payload = {
                    userId: found._id,
                    userEmail: found.email,
                    userMobile: found.mobile,
                }
                const token = jwt.sign(payload, JWT_SECRET)
                logger.info('Login Successfully')
                res.status(200).json({ msg: 'Login Successfully', data: token })
            } else {
                logger.warn('Password is Incorrect');
                res.status(401).json({ msg: 'Password is Incorrect' })
            }
        } else {
            logger.warn('User Not Found')
            res.status(404).json({ msg: 'User Not Found' })
        }
    } catch (error) {
        logger.error('Error In Login', error)
        res.status(500).json({ msg: 'Error In Login' })
    }
}

module.exports = userController