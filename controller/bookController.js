const { Books } = require('../model')
const { logger } = require('../middleware')

class bookController { }
bookController.create = async (req, res) => {
    let { body: { title, author, language } } = req
    try {
        let found = await Books.findOne({ $and: [{ title }, { author }, { language }] })
        if (!found) {
            const saved = await Books.create(req.body)
            logger.info('Registered Successfully')
            res.status(201).json({ msg: 'Registered Successfully', data: saved })
        } else {
            logger.error('Already existed Book')
            res.status(208).json({ msg: 'Already existed Book' })
        }
    } catch (error) {
        res.status(500).send({ error: 'Error occurred while creating the book' })
    }
}
bookController.find = async (req, res) => {
    try {
        const founded = await Books.find()
        logger.info('Get all books successfully')
        res.status(200).json(founded)
    } catch (error) {
        logger.error('Error occurred while finding the books')
        res.status(500).send({ error: 'Error occurred while finding the books' })
    }
}
bookController.findOne = async (req, res) => {
    try {
        const founded = await Books.findOne({ _id: req.params.id })
        logger.info('Get the book successfully')
        res.status(200).json(founded)
    } catch (error) {
        logger.error('Error occurred while finding the book')
        res.status(500).send({ error: 'Error occurred while finding the book' })
    }
}
bookController.updateOne = async (req, res) => {
    try {
        const founded = await Books.updateOne({ _id: req.params.id }, req.body)
        logger.info('Updated the book successfully')
        res.status(200).json(founded)
    } catch (error) {
        logger.error('Error occurred while updating the book')
        res.status(500).send({ error: 'Error occurred while updating the book' })
    }
}
bookController.updateMany = async (req, res) => {
    try {
        const founded = await Books.updateMany({ title: { $in: req.body.title } }, { rate: req.body.rate })
        logger.info('Updated the books successfully')
        res.status(200).json(founded)
    } catch (error) {
        logger.error('Error occurred while updating the books')
        res.status(500).send({ error: 'Error occurred while updating the books' })
    }
}
bookController.deleteById = async (req, res) => {
    const bookId = req.params.id;
    try {
        const deletedBook = await Books.findByIdAndDelete(bookId)
        if (deletedBook) {
            logger.info('Deleted the book successfully')
            res.status(200).json({ deleted: deletedBook, msg: 'Deleted Successfully' })
        } else {
            logger.info('Book not found')
            res.status(404).json({ error: 'Book not found' })
        }
    } catch (error) {
        logger.error('Error occurred while deleting the book')
        res.status(500).json({ error: 'Error occurred while deleting the book' })
    }
}
bookController.deleteByToken = async (req, res) => {
    try {
        const bookId = req.user
        const deletedBook = await Books.findByIdAndDelete({ _id: bookId })
        if (deletedBook) {
            logger.info('Deleted Successfully')
            res.status(200).json({ deleted: deletedBook, msg: 'Deleted Successfully' })
        } else {
            logger.error('Book not found')
            res.status(404).json({ error: 'Book not found' })
        }
    } catch (error) {
        logger.error('Error occurred while deleting the book')
        res.status(500).json({ error: 'Error occurred while deleting the book' })
    }
}
module.exports = bookController
