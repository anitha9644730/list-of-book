const express = require('express')
const routes = express.Router()
const { bookController } = require('../controller')
const { tokenValidator } = require('../middleware')

routes.post('/add', bookController.create)
routes.get('/find', bookController.find)
routes.get('/find/:id', bookController.findOne)
routes.put('/update/:id', bookController.updateOne)
routes.put('/update', bookController.updateMany)
routes.delete('/delete/:id', tokenValidator, bookController.deleteById)



module.exports = routes
