
module.exports = app => {
    app.use('/book', require('./bookRoutes'))
    app.use('/user', require('./userRoutes'))
}