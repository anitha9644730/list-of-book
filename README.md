
# clone project

```git clone https://gitlab.com/anitha9644730/list-of-book.git```

# project setup

step 1: ```npm install```
step 2: add .env file in project
step 3: add PORT, JWT_SECRET, DB_URL, DB_NAME variables in .env file

# run project

```npm start```