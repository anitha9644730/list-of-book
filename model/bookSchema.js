const { Schema, model } = require("mongoose")

module.exports = model('Books', new Schema({
    title: {
        type: String,
        required: true
    },
    author: {
        type: String
    },
    publicationYear: {
        type: Number,
        min: 1000,
        max: 9999
    },
    language: {
        type: String,
        required: true
    },
    rate: {
        type: Number
    }
},
    {
        collection: 'book',
        timestamps: true
    }
))
