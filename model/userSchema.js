const { Schema, model } = require("mongoose")

module.exports = model('User', new Schema({
    name: {
        type: String,
        required: true
    },
    mobile: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['Active', 'InActive', 'Deleted'],
        default: 'Active'
    }
},
    {
        collection: 'user',
        timestamps: true
    }
))
