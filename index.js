const express = require("express")
const app = express()
const routers = require('./routes')
const { log } = console
const { connect, connection, set } = require('mongoose')
require("dotenv").config()

const cookieParser = require("cookie-parser")
const { PORT, DB_URL, DB_NAME } = process.env

app.listen(PORT, () => log('Server On:', PORT))

// app.use((req, res, next) => {
//     console.log('Method:', req.method, req.url, 'time:', new Date())
//     logger.er
//     next()
// })
app.use(express.json())
app.use(cookieParser())
routers(app)

set('strictQuery', false)
connect(DB_URL + DB_NAME)
connection.once('open', () => log('DB Connect:', DB_NAME))
